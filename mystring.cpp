//
// Created by Marc Szymkowiak
//

#include "mystring.h"

/**
 * count words in a string
 * @param strings
 * @return number of words
 */
int wordCount(char strings[]) {
    char* stringPointer = strings;
    int numberOfWords = 0;
    int lastCharIsEmpty = 1;    //"   aa \n \t  bcdvd asd\0"

    while(*stringPointer != '\0') {
        if(*stringPointer == ' ' || *stringPointer == '\t' || *stringPointer == '\n') {
            lastCharIsEmpty = 1;
        }
        else {
            if(lastCharIsEmpty == 1) {
                numberOfWords++;
                lastCharIsEmpty = 0;
            }
        }
        stringPointer++;
    }
    return numberOfWords;
}

/**
 * compare two string
 * @param firstString
 * @param secondString
 * @return  0 if equal, 1 if first > second, -1 if first < second
 */
int strcmp_ign_ws(char firstString[], char secondString[]) {
    char* first = firstString;
    char* second = secondString;

    while(*first != '\0' || *second != '\0') {
        while(*first == ' ') {
            if(*first != '\0') {
                first++;
            }
        }
        while(*second == ' ') {
            if(*second != '\0') {
                second++;
            }
        }

        if(*first < *second) {
            return -1;
        }
        if(*first > *second) {
            return 1;
        }

        if(*first != '\0') {
            first++;
        }
        if(*second != '\0') {
            second++;
        }
    }

    return 0;
}

