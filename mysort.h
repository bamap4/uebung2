//
// Created by Marc Szymkowiak && Basit Rehman
//

#ifndef UEBUNG2_MYSORT_H
#define UEBUNG2_MYSORT_H

#include <cstdio>

template <typename T>
void insertion_sort(T* begin, T* end)
{
    for(T* right = begin+1; right < end; ++right) {
        T tmpValue = *right;

        T* left;

        for(left = right-1;left >= begin && tmpValue < *left; --left) {
            *(left+1) = *left;
        }
        *(left+1) = tmpValue;
    }
}


#endif //UEBUNG2_MYSORT_H
