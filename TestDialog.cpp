//
// Created by Basit on 29.11.2017.
//

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "mystring.h"
#include "mysort.h"

#define MAX_CHAR 50

using namespace std;

int countWords();
int stringCompare();
int intSort();
int charSort();

const int	WORD_COUNT			= 1;
const int	STRING_COMPARE		= 2;
const int	INSERTION_SORT_CHAR	= 3;
const int   INSERTION_SORT_INT  = 4;
const int	ENDE				= 0;

const char	PROGRAMM_ENDE []		= "Das Programm wurde beendet! \n";
const char	DEFAULT_EINGABE []		= "Dies war keine gültige Eingabe! \n";

int ende = -1;
int funktion;

int switchFunktion(int funktion){
    printf("\n---------------------\n");
    printf("1: Word Count \n");
    printf("2: String Compare \n");
    printf("3: Insertion-Sort (char) \n");
    printf("4: Insertion-Sort (int)\n");
    printf("0: Programm beenden! \n");
    printf("---------------------\n");

    scanf("%i" , &funktion);

    switch (funktion) {

        case WORD_COUNT:
            countWords();
            break;

        case STRING_COMPARE:
            stringCompare();
            break;

        case INSERTION_SORT_CHAR:
            charSort();
            break;

        case INSERTION_SORT_INT:
            intSort();
            break;

        case ENDE:
            ende = 0;
            printf("%s", PROGRAMM_ENDE);
            break;

        default:
            printf("%s", DEFAULT_EINGABE);
            break;
    }
    return 0;
}

int countWords(){
    char strings[MAX_CHAR];

    printf("Geben Sie Ihre Zeichenfolge ein:");
    while(getchar() != '\n');
    fgets(strings, MAX_CHAR, stdin);

    printf("Anzahl der Woerter: %d\n", wordCount(strings));

    return 0;
}

int stringCompare(){
    char strings[MAX_CHAR];
    char strings2[MAX_CHAR];

    printf("Geben Sie Ihre Zeichenfolge ein:");
    while(getchar() != '\n');
    fgets(strings, MAX_CHAR, stdin);

    printf("Geben Sie Ihre Zeichenfolge ein:");
    fgets(strings2, MAX_CHAR, stdin);

    switch(strcmp_ign_ws(strings, strings2)){

        case 0:
            printf("Die Zeichenfolgen sind gleich");
            break;
        case 1:
            printf("%s ist groesser als %s", strings, strings2);
            break;
        case -1:
            printf("%s ist kleiner als %s", strings, strings2);
            break;
    }
}

int charSort(){
    char c[MAX_CHAR] = {};
    char values[MAX_CHAR] = {};
    int j=0;


    printf("Geben Sie Ihre Zeichenfolge durch Leerzeichen getrennt ein:");
    while(getchar() != '\n');
    fgets(c, MAX_CHAR, stdin);

    for(int i=0; i < MAX_CHAR; i++) {
        if(c[i] == '\0') {
            i = MAX_CHAR;
        }
        if(c[i] != ' ' && c[i] != '\n' && i != MAX_CHAR) {
            values[j] = c[i];
            j++;
        }
    }


    char * ende =  values+j;
    insertion_sort(values, ende);

    printf("sorted: %s\n", values);
}

int intSort(){
    int number = 0, val;
    char buffer[256];

    printf("Wie viele Zahlen wollen sie eingeben? ");
    while(getchar() != '\n');
    fgets (buffer, 256, stdin);
    number = atoi (buffer);

    int values[number] = {};

    for(int i=0;i<number;i++) {
        char buf[256] = {};
        printf("Geben sie Zahl %d an: ",i);
        fgets (buf, 256, stdin);
        val = atoi(buf);
        values[i] = val;
    }


    int laenge = sizeof(values)/sizeof(int);
    int * ende =  values+laenge;
    insertion_sort(values, ende);

    printf("\nSortiert: \n");

    for(int i=0; i<number;i++) {
        printf("%d ",values[i]);
    }
}

int main(void){
    while(ende != 0){
        switchFunktion(funktion);
    }
}
